#pragma once

#ifndef WINDOWS_UTILS_H
#define WINDOWS_UTILS_H

#include <windows.h>
#include <vector>
#include "WindowsDataValue.h"
#include "stdafx.h"

namespace WindowsUtilsN 
{

	class WindowsUtils
	{
	public:

		WindowsUtils() 
		{
		}

		~WindowsUtils()
		{
		}

		static bool HasChanged() 
		{ 
			std::wstring temp = GetActiveWindowTitle();

			if (current_window_name != temp)
			{
				current_window_name = temp;
				return true;
			}

			return false;
		}

		static std::string GetCurrentWindow() 
		{
			const std::string s(current_window_name.begin(), current_window_name.end());
			return s;
		}

		static std::vector<WindowsDataValue> GetListAllWindows() 
		{
			titles.clear();
			counter_windows = 0;
			EnumWindows(WindowsEnumProcessor, reinterpret_cast<LPARAM>(&titles));
			return titles;
		}

		static std::string GetListAllWindowsToString() 
		{
			std::string retVal = "";
			for (const auto& title : GetListAllWindows())
			{
				const std::string s(title.Name.begin(), title.Name.end());
				retVal += "Title Number " + std::to_string(title.Id) + " " + s + "\n";
			}
			return retVal;
		}

	private:

		static int counter_windows;

		static std::wstring current_window_name;

		static std::vector<WindowsDataValue> titles;

		static BOOL CALLBACK WindowsEnumProcessor(HWND hwnd, LPARAM lParam)
		{
			const DWORD TITLE_SIZE = 1024;
			WCHAR windowTitle[TITLE_SIZE];

			GetWindowTextW(hwnd, windowTitle, TITLE_SIZE);

			int length = ::GetWindowTextLength(hwnd);
			std::wstring title(&windowTitle[0]);
			if (!IsWindowVisible(hwnd) || length == 0 || title == L"Program Manager") {
				return TRUE;
			}

			counter_windows++;

			// Retrieve the pointer passed into this callback, and re-'type' it.
			// The only way for a C API to pass arbitrary data is by means of a void*.
			std::vector<WindowsDataValue>& titles =
				*reinterpret_cast<std::vector<WindowsDataValue>*>(lParam);
			titles.emplace_back(counter_windows, title, L"", (bool)(current_window_name == title));

			return TRUE;
		}

		static std::wstring GetActiveWindowTitle()
		{

			const DWORD TITLE_SIZE = 1024;
			WCHAR windowTitle[TITLE_SIZE];

			HWND hwnd = GetForegroundWindow(); // get handle of currently active window
			GetWindowTextW(hwnd, windowTitle, TITLE_SIZE);

			return windowTitle;
		}

	};
	
	int WindowsUtils::counter_windows = 0;

	std::wstring WindowsUtils::current_window_name = L"";

	std::vector<WindowsDataValue> WindowsUtils::titles;

}

#endif // !WINDOWS_UTILS_H


