#ifndef EncryptB64_H
#define EncryptB64_H

#include <vector>
#include <string>
#include <iostream>
#include "base64.h"

namespace EncryptB64
{
	const std::string  &SALT1 = "2{m>qF,TSRYcpyZY";
	const std::string  &SALT2 = "xwRC7~LPv@?Z,G#N";
	const std::string  &SALT3 = "DYw6vvx7{2a+qDA3";
	const std::string  &SALT4 = "NX98Ac77McLWzRRC";
	const std::string  &SALT5 = "xdyH44TNc5wGEbLP";
	const std::string  &SALT6 = "wMSq7bBb3HhabAej";

	std::string base64_encode(const std::string &s)
	{
		return Base64::base64_encode(reinterpret_cast<const unsigned char*>(s.c_str()), s.length());
	}

	std::string TakeOutEnd(std::string s, const std::string &what)
	{
		if (what.empty())
			return s;

		int lenNumberWhat = (int)what.length();
		int lenNumberS = (int)s.length();

		std::string firstPart = (s.substr(0, lenNumberS - lenNumberWhat));;

		return firstPart;
	}

	std::string TakeOutStart(std::string s, const std::string &what)
	{
		if (what.empty())
			return s;

		int lenNumber = (int)what.length();

		std::string lastPart = (s.substr(lenNumber));

		return lastPart;
	}

	std::string TakeOutIndex(std::string s, const int &index, const std::string &what = "X")
	{
		if (index < 1)
		{
			return TakeOutStart(s, what);
		}

		int lenNumber = (int)what.length();

		std::string firstPart = (s.substr(0, index));;
		std::string lastPart = (s.substr(index + lenNumber));

		return firstPart + lastPart;
	}

	std::string EncryptB64(std::string s)
	{
		s = SALT1 + s + SALT2 + SALT3;
		s = base64_encode(s);
		s.insert(25, SALT4);
		s.insert(13, SALT5);
		s = SALT1 + SALT2 + s + SALT3;
		s = base64_encode(s);
		s = SALT2 + s + SALT3 + SALT1;
		s = base64_encode(s);
		s.insert(3, SALT6);
		s.insert(4, SALT5);
		s = base64_encode(s); 
		return s;
	}

	std::string DecryptB64(std::string s)
	{
		s = Base64::base64_decode(s);   // s = base64_encode(s); 
		s = TakeOutIndex(s, 4, SALT5);  // s.insert(4, SALT5);
		s = TakeOutIndex(s, 3, SALT6);  // s.insert(3, SALT6);
		s = Base64::base64_decode(s);   // s = base64_encode(s);
		s = TakeOutEnd(s, SALT1); s = TakeOutEnd(s, SALT3); s = TakeOutStart(s, SALT2);   // s = SALT2 + s + SALT3 + SALT1;
		s = Base64::base64_decode(s);   // s = base64_encode(s); 
		s = TakeOutEnd(s, SALT3); s = TakeOutStart(s, SALT1); s = TakeOutStart(s, SALT2);   // s = SALT1 + SALT2 + s + SALT3;
		s = TakeOutIndex(s, 13, SALT5); // s.insert(13, SALT5);	
		s = TakeOutIndex(s, 25, SALT4); // s.insert(25, SALT4);
		s = Base64::base64_decode(s);   // s = base64_encode(s); 
		s = TakeOutEnd(s, SALT3); s = TakeOutEnd(s, SALT2); s = TakeOutStart(s, SALT1);   // s = SALT1 + s + SALT2 + SALT3;
		return s;
	}

}

#endif
