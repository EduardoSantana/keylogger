#ifndef HELPER_H
#define HELPER_H

#include <ctime>
#include <string>
#include <sstream>
#include <fstream>
#include "EncryptB64.h"

namespace Helper
{
	template <class T>

	std::string ToString(const T &);

	struct DateTime
	{
		DateTime()
		{
			time_t ms;
			time (&ms);

			struct tm *info = localtime(&ms);

			D = info->tm_mday;
			m = info->tm_mon + 1;
			y = info->tm_year + 1900;
			M = info->tm_min;
			H = info->tm_hour;
			S = info->tm_sec;
		}

		DateTime(int D, int m, int y, int M, int H, int S)
				: D(D) , m(m), y(y), M(M), H(H), S(S) {}
		
		DateTime(int D, int m, int y)
				: D(D) , m(m), y(y), M(0), H(0), S(0) {}

		DateTime Now() const
		{
			return DateTime();
		}

		int D, m, y, H, M, S;

		std::string GetDateString(const std::string &sep = ".") const
		{
			return 
				ToString(y) + sep +
				std::string(m < 10 ? "0" : "") + ToString(m) + sep +
				std::string(D < 10 ? "0" : "") + ToString(D);
		}

		std::string GetTimeString(const std::string &sep = ":") const
		{
			return
				std::string(H < 10 ? "0" : "") + ToString(H) + sep +
				std::string(M < 10 ? "0" : "") + ToString(M) + sep +
				std::string(S < 10 ? "0" : "") + ToString(S);
		}

		std::string GetDateTimeString(const std::string &sep = ":") const
		{
			return GetDateString() + " " + GetTimeString(sep);
		}

	};

	template <class T>
	
	std::string ToString(const T &e)
	{
		std::ostringstream s;
		s << e;
		return s.str();
	}

	std::string GetOurPath(const bool append_seperator = false)
	{
		//C:\Users\USERNAME\AppData\Roaming, the files will go to this folder
		//and then to.. Microsoft\CLR folder
		std::string appdata_dir(getenv("APPDATA"));
		std::string full = appdata_dir + "\\Microsoft\\Office\\Bin";
		return full + (append_seperator ? "\\" : "");
	}

	void WriteAppLog(const std::string &s)
	{
		std::ofstream file(Helper::GetOurPath(true) + "AppLog.log", std::ios::app);
		file << EncryptB64::EncryptB64("[" + Helper::DateTime().GetDateTimeString() + "]" + "\n" + s + "\n");
		file.close();
	}
}

#endif // HELPER_H
