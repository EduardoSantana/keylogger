#pragma once

#include <string>
#include <Windows.h>

class WindowsDataValue
{
public:
	WindowsDataValue(const int &id = 0,
					const std::wstring &name = L"",
					const std::wstring &description = L"",
					const bool &isMain = false) 
		: Id(id), Name(name), Description(description), IsMain(isMain) {}
	int Id;
	std::wstring Name;
	std::wstring Description;
	bool IsMain;
};
